-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost    Database: catur
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_pertandingan`
--

DROP TABLE IF EXISTS `tb_pertandingan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tb_pertandingan` (
  `id` int NOT NULL,
  `id_peserta_a` int NOT NULL,
  `id_peserta_b` int NOT NULL,
  `hasil` float NOT NULL,
  `mulai` varchar(10) DEFAULT NULL,
  `selesai` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_peserta_a` (`id_peserta_a`),
  KEY `id_peserta_b` (`id_peserta_b`),
  CONSTRAINT `tb_pertandingan_ibfk_1` FOREIGN KEY (`id_peserta_a`) REFERENCES `tb_peserta` (`id`),
  CONSTRAINT `tb_pertandingan_ibfk_2` FOREIGN KEY (`id_peserta_b`) REFERENCES `tb_peserta` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_pertandingan`
--

LOCK TABLES `tb_pertandingan` WRITE;
/*!40000 ALTER TABLE `tb_pertandingan` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_pertandingan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_peserta`
--

DROP TABLE IF EXISTS `tb_peserta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tb_peserta` (
  `id` int NOT NULL,
  `nama_depan` varchar(255) DEFAULT NULL,
  `nama_belakang` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `no_telp` varchar(255) DEFAULT NULL,
  `id_gelar` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_gelar` (`id_gelar`),
  CONSTRAINT `tb_peserta_ibfk_1` FOREIGN KEY (`id_gelar`) REFERENCES `tb_rank` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_peserta`
--

LOCK TABLES `tb_peserta` WRITE;
/*!40000 ALTER TABLE `tb_peserta` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_peserta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_rank`
--

DROP TABLE IF EXISTS `tb_rank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tb_rank` (
  `id` int NOT NULL,
  `gelar` varchar(255) DEFAULT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_rank`
--

LOCK TABLES `tb_rank` WRITE;
/*!40000 ALTER TABLE `tb_rank` DISABLE KEYS */;
INSERT INTO `tb_rank` VALUES (1,'Super GM','ELO di atas 2700'),(2,'GM','Grandmaster, ELO di atas 2600'),(3,'kuda','Telur');
/*!40000 ALTER TABLE `tb_rank` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-22 16:31:28
